// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright (C) 2021 ÉO
// This file is part of ÉO Paysages - Éditeur.

// ÉO Paysages - Éditeur is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// ÉO Paysages - Éditeur is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with ÉO Paysages - Éditeur.  If not, see <https://www.gnu.org/licenses/>.
import Ajv from 'ajv';

const ajv = new Ajv();

// TODO: Hedges might be incomplete

/**
 * JSON schema used by the AJV validator.
 */
const scenario = {
	"type": "object",
	"required": [
		"title",
		"color",
		"formatVersion",
		"turbinePositions"
	],
	"properties": {
		"title": {
			"type": "string"
		},
		"color": {
			"type": "string",
			"minLength": 4,
			"maxLength": 9
		},
		"formatVersion": {
			"type": "integer",
			"minimum": 0
		},
		"turbinePositions": {
			"type": "array",
			"items": [
				{
					"allOf": [
						{
							"type": "object",
							"required": [
								"lat",
								"lon"
							],
							"properties": {
								"lat": {
									"type": "number"
								},
								"lon": {
									"type": "number"
								}
							}
						}
					]
				}
			]
		},
		"turbineDimensions": {
			"type": "object"
		},
		"hedges": {
			"type": "array",
			"items": [
				{
					"allOf": [
						{
							"type": "array",
							"items": [
								{
									"allOf": [
										{
											"type": "object",
											"required": [
												"lat",
												"lon"
											],
											"properties": {
												"lat": {
													"type": "number"
												},
												"lon": {
													"type": "number"
												}
											}
										}
									]
								}
							]
						}
					]
				}
			]
		}
	},
	"title": "scenario"
};

/**
 * Check if a deserialized file is a valid scenario.
 */
export const validateScenario = ajv.compile(scenario);
