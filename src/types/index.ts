// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright (C) 2021 ÉO
// This file is part of ÉO Paysages - Éditeur.

// ÉO Paysages - Éditeur is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// ÉO Paysages - Éditeur is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with ÉO Paysages - Éditeur.  If not, see <https://www.gnu.org/licenses/>.
import { hashBinary } from 'spark-md5';
import { validateScenario } from './schemas';

/**
 * Contents of a scenario.
 * This is the class that gets serialized into exported files.
 */
export class ScenarioData {
	public title: string;

	formatVersion = 1;

	static nextColor = 0;
	public color: string;

	public turbinePositions: Geopoint[] = [];
	public turbineDimensions?: TurbineDimensions = undefined;
	public hedges: Geopoint[][] = [];

	constructor(title: string) {
		this.title = title;
		// Find a default color
		this.color = Colors.colors[ScenarioData.nextColor];
		ScenarioData.nextColor = (ScenarioData.nextColor + 1) % Colors.colors.length;
	}

	/**
	 * Check if this scenario is considered valid.
	 * The export button should be disabled if any scenario is invalid.
	 */
	public isValid(): boolean {
		return this.title.trim().length > 0
			&& this.turbinePositions.length > 0
			&& (this.turbineDimensions === undefined || this.turbineDimensions.isValid());
	}
}

/**
 * Default colors assigned to new scenarios
 */
class Colors {
	static readonly colors: string[] = [
		'#ff0000',
		'#fa26a0',
		'#f8d210',
		'#2ff3e0',
		'#b68d40',
		'#f88379',
		'#2a4dea',
		'#b61cea',
		'#81b622',
		'#ff8300',
	];
}

/**
 * Container for ScenarioData.
 * Adds editor-specific metadata and handles (de)serialization.
 */
export class Scenario {
	public scenarioData: ScenarioData;

	public show_in_editor_map = true;

	constructor(title: string) {
		this.scenarioData = new ScenarioData(title);
	}
	
	public displayName(): string {
		if (this.scenarioData.title.trim().length == 0)
			return "Sans titre";
		return this.scenarioData.title.trim();
	}

	/**
	 * Check if this scenario is considered valid.
	 * The export button should be disabled if any scenario is invalid.
	 */
	public isValid(): boolean {
		return this.scenarioData.isValid();
	}
	
	public toJson(): string | undefined {
		if (this.isValid()) {
			return JSON.stringify(this.scenarioData, null, 1);
		}
		return undefined;
	}

	public static fromJson(textData: string): Scenario | undefined {
		const json: unknown = JSON.parse(textData);

		if ( !validateScenario(json) ) {
			console.log('File failed scenario validation');
			return undefined;
		}
		const jsonObj = json as Record<string, unknown>;

		const data: ScenarioData = new ScenarioData('');
		
		// TODO: Check types
		data.title = jsonObj.title as string;
		data.color = jsonObj.color as string;
		const turbinePositions: Geopoint[] = [];
		for (const position of jsonObj.turbinePositions as unknown[]) {
			turbinePositions.push(position as Geopoint);
		}
		data.turbinePositions = turbinePositions;

		data.turbineDimensions = TurbineDimensions.fromJsonObj(jsonObj.turbineDimensions);

		const hedges: Geopoint[][] = [];
		for (const jsonHedge of jsonObj.hedges as unknown[]) {
			const hedge: Geopoint[] = [];
			for (const position of jsonHedge as unknown[]) {
				hedge.push(position as Geopoint);
			}
			hedges.push(hedge);
		}
		data.hedges = hedges;

		const scenario = new Scenario('');
		scenario.scenarioData = data;
		return scenario;
	}
}

/**
 * Handles common files referenced by multiple scenarios.
 * In the current version of the format, this is unused.
 * This could be used to store natural zones data.
 */
export class FileManager {
	private files: Map<string, File> = new Map();

	/**
	* @returns the unique key for this file
	*/
	public addFile(file: File): string {
		const fileHash = FileManager.getHash(file);

		if (this.files.has(fileHash)) {
			console.log('File already loaded');
		} else {
			this.files.set(fileHash, file);
		}

		return fileHash;
	}

	/**
	 * Get a file from its unique key.
	 * @param key the unique key
	 * @returns the File, or undefined if not found
	 */
	public get(key: string): File | undefined {
		return this.files.get(key);
	}

	/**
	 * Unloads a file.
	 * @param key the file's unique key
	 */
	public remove(key: string): void {
		this.files.delete(key);
	}

	/**
	 * @returns all loaded files, mapped by their key.
	 */
	public getAllFiles(): ReadonlyMap<string, File> {
		return this.files;
	}

	private static getHash(file: File): string {
		console.warn('File hashing is not implemented! Returning filename and size as a string instead.');
		return file.name + file.size; // TODO
		// return hashBinary(reader);
	}

	private static readonly FILENAME_ALLOWED_CHARS = /([a-z]|[0-9]|-|_)/i;
	/**
	 * Removes illegal characters from a file name. 
	 * Make sure you pass a name, not a path as slashes 
	 * will be removed.
	 */
	public static sanitizeFileName(fileName: string): string {
		let res = "";
		for (const c of fileName) {
			if (c.match(FileManager.FILENAME_ALLOWED_CHARS)) {
				res += c;
			}
			else if (c == ' ') {
				res += '_';
			}
		}
		return res;
	}

	/**
	 * @returns lowercase file extension, or undefined if the file doesn't have one.
	 */
	static getExtension(file: File): string | undefined {
		return FileManager.getExtensionFromString(file.name);
	}

	/**
	 * @returns lowercase file extension, or undefined if the file doesn't have one.
	 */
	static getExtensionFromString(file: string): string | undefined {
		const dot = file.lastIndexOf('.');
		if (dot < 0) {
			return undefined;
		}
		return file.substr(dot + 1).toLowerCase();
	}

	static parseKmlGeoCoordinatesElement(element: Element): Geopoint | undefined {
		const elem = element as Element;
		if (elem.textContent) {
			return FileManager.parseKmlGeoCoordinatesString(elem.textContent);
		} else {
			return undefined;
		}
	}

	static parseKmlGeoCoordinatesString(str: string): Geopoint | undefined {
		const parts = str.split(',');
		if (parts.length === 2) {
			const lat = Number.parseFloat(parts[1]);
			const lon = Number.parseFloat(parts[0]);
			if (Number.isNaN(lat) || Number.isNaN(lon)) {
				return undefined;
			}
			return new Geopoint(lat, lon);
		}
		return undefined;
	}

	public static parsePositions = async(file: File): Promise<Geopoint[]> => {
		const extension = FileManager.getExtension(file);

		switch (extension) {
			case "kml": {
				const res: Geopoint[] = [];

				const textData = await file.text();
				const parser = new DOMParser();
				const doc = parser.parseFromString(textData, "application/xml");

				const placemarks = doc.getElementsByTagName("Placemark");
				for (const point of placemarks)
				{
					const coords = point.getElementsByTagName("coordinates");
					if (coords.length === 1) {
						const geopoint = FileManager.parseKmlGeoCoordinatesElement(coords[0]);
						if (geopoint) {
							res.push(geopoint);
						}
					}
				}

				return res;
			}
			case undefined: {
				console.log('Parse positions: File has no extension');
				alert('Le fichier n\'a pas d\'extension.');
				return [];
			}
			default:
				console.log('Parse positions: Unsupported file format: "'+extension+'"');
				alert('Format de fichier non supporté: "'+extension+'"');
				return [];
		}
	}

	public static parseHedges = async(file: File): Promise<Geopoint[][]> => {
		const extension = FileManager.getExtension(file);

		switch (extension) {
			case "kml": {
				const res: Geopoint[][] = [];

				const textData = await file.text();
				const parser = new DOMParser();
				const doc = parser.parseFromString(textData, "application/xml");

				const lineStrings = doc.getElementsByTagName("LineString");
				for (const point of lineStrings)
				{
					const line: Geopoint[] = [];

					const coords = point.getElementsByTagName("coordinates");
					if (coords.length === 1) {
						if (coords[0].textContent) {
							const points = coords[0].textContent.split(' ');
							for (const point of points) {
								const geopoint = FileManager.parseKmlGeoCoordinatesString(point);
								if (geopoint) {
									line.push(geopoint);
								}
							}
						}
					}

					res.push(line);
				}

				return res;
			}
			case undefined: {
				console.log('Parse hedges: File has no extension');
				alert('Le fichier n\'a pas d\'extension.');
				return [];
			}
			default:
				console.log('Parse hedges: Unsupported file format: "'+extension+'"');
				alert('Format de fichier non supporté: "'+extension+'"');
				return [];
		}
	}

}

/**
 * WGS84 coordinates.
 * This does not contains any altitude data.
 */
export class Geopoint {
	public lat: number;
	public lon: number;

	constructor(latitude: number, longitude: number) {
		this.lat = latitude;
		this.lon = longitude;
	}

	public equals(other: Geopoint): boolean {
		return this.lat === other.lat && this.lon === other.lon;
	}

	public toString = () : string => '{lat:'+this.lat+', lon:'+this.lon+'}'

	public static removeDoubles(array: Geopoint[]): Geopoint[] {
		return array.filter((val, id, arr) =>
			!arr.filter((v, i) => i < id && val.equals(v)).length);
	}
}

export class TurbineDimensions {
	public height = 100;
	public diameter = 4;
	public rotorDiameter = 100;

	public static fromJsonObj(jsonObj: unknown): TurbineDimensions | undefined {
		const cast = jsonObj as TurbineDimensions;
		if (jsonObj === undefined) {
			return undefined;
		}
		const res = new TurbineDimensions();
		res.height = cast.height;
		res.diameter = cast.diameter;
		res.rotorDiameter = cast.rotorDiameter;
		return res;
	}

	public static readonly HEIGHT_MIN: number = 1.0;
	public static readonly HEIGHT_MAX: number = 300.0;
	public static readonly DIAMETER_MIN: number = 1.0;
	public static readonly DIAMETER_MAX: number = 30.0;
	public static readonly ROTOR_DIAMETER_MIN: number = 1.0;
	public static readonly ROTOR_DIAMETER_MAX: number = 300.0;

	public heightValid(): boolean {
		return this.height >= TurbineDimensions.HEIGHT_MIN && this.height <= TurbineDimensions.HEIGHT_MAX;
	}

	public diameterValid(): boolean {
		return this.diameter >= TurbineDimensions.DIAMETER_MIN && this.diameter <= TurbineDimensions.DIAMETER_MAX;
	}

	public rotorDiameterValid(): boolean {
		return this.rotorDiameter >= TurbineDimensions.ROTOR_DIAMETER_MIN && this.rotorDiameter <= TurbineDimensions.ROTOR_DIAMETER_MAX;
	}

	public isValid(): boolean {
		return this.heightValid()
			&& this.diameterValid()
			&& this.rotorDiameterValid();
	}
}
