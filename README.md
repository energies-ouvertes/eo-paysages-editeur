# ÉO Paysages - Éditeur

Éditeur de fichiers de scénarios pour l'application [ÉO Paysages](https://gitlab.com/energies-ouvertes/eo-paysages).

L'application peut être utilisée sur [GitLab Pages](https://energies-ouvertes.gitlab.io/eo-paysages-editeur).

## Installation
```
npm install
```

### Compiler et exécuter avec hot-reloads (pour le développement)
```
npm run serve
```

### Compiler et minimiser (pour la production)
```
npm run build
```

