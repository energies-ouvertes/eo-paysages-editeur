module.exports = {
	publicPath: process.env.NODE_ENV === 'production'
		? '/eo-paysages-editeur/'
		: '/',
		
	pages: {
		index: {
			entry: 'src/main.ts',
			title: 'ÉO Paysages - Éditeur de scénarios',
		},
	}
}
